import { render, fireEvent } from '@testing-library/svelte';
import Hello from './Hello.svelte';
import { describe, it, expect } from 'vitest';

describe('hello.svelte', () => {
	it('renders correct text', () => {
		const { getByText } = render(Hello);
		expect(getByText('Hi there')).toBeDefined();
	});
	it('renders correct text', () => {
		const { getByText } = render(Hello, { name: 'Paul' });
		expect(getByText('Hi Paul')).toBeDefined();
		expect(getByText('Count:0')).toBeDefined();
		expect(getByText('increment', { exact: false })).toBeDefined();
	});
	it('button fires correctly', async () => {
		const { getByText } = render(Hello);
		await fireEvent.click(getByText('increment', { exact: false }));
		expect(getByText('Count:1')).toBeDefined();
		await fireEvent.click(getByText('decrement', { exact: false }));
		expect(getByText('Count:0')).toBeDefined();
	});
});
